#! /usr/bin/env python3

import subprocess
import sys

if len(sys.argv) <= 1:
    sys.exit(1)

try:
    temp = int(open("/sys/bus/platform/devices/thinkpad_hwmon/temp1_input", "r").read())
except:
    sys.exit(1)

if temp <= 60000:
    args = sys.argv
    args[0] = "cat"
    out = subprocess.Popen(args)
else:
    print("not cool")
